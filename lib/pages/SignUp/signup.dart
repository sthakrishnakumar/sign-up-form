import 'package:demo_app/pages/Login/login.dart';
import 'package:demo_app/pages/formfield.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';

class SignUp extends StatefulWidget {
  const SignUp({Key? key}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController addressController = TextEditingController();

  GlobalKey<FormState> formkey = GlobalKey<FormState>();
  double gap = 8;
  bool isHidden = true;
  IconData icon = Icons.visibility_off_outlined;

  bool buttonPressed = false;

  String? genderError;
  String? provinceError;
  String? districtError;

  _validateForm() {
    bool _isValid = formkey.currentState!.validate();

    if (selectedGender == null) {
      setState(() {
        genderError = 'Please Select Gender';
      });
      _isValid = false;
    }

    if (selectedProvince == null) {
      setState(() {
        provinceError = 'Please Select Province';
      });
      _isValid = false;
    }

    if (selectedDistrict == null) {
      setState(() {
        districtError = 'Please Select District';
      });
      _isValid = false;
    }

    if (_isValid) {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: ((context) => const Login()),
        ),
      );
    }
  }

  String? selectedGender;
  String? selectedProvince;
  String? selectedDistrict;
  List<String> districts = [];

  List<String> gender = ['Male', 'Female', 'Others'];

  List<String> provinces = [
    "Province No. 1",
    "Province No. 2",
    "Province No. 3",
    "Province No. 4",
    "Province No. 5"
  ];

  List<String> provinceOneDistrict = [
    'Bhojpur',
    'Dhankuta',
    'Ilam',
    'Jhapa',
    'Khotang',
    'Morang',
    'Okhaldhunga',
    'Panchthar',
    'Sankhuwasabha',
    'Solukhumbu',
    'Sunsari',
    'Taplejung',
    'Terhathum',
    'Udayapur',
  ];
  List<String> provinceTwoDistrict = [
    'Sarlahi',
    'Dhanusha',
    'Bara',
    'Rautahat',
    'Saptari',
    'Siraha',
    'Mahottari',
    'Parsa',
  ];
  List<String> provinceThreeDistrict = [
    'Bhaktapur',
    'Chitwan',
    'Dhading',
    'Dolakha',
    'Kathmandu',
    'Kavrepalanchok',
    'Lalitpur',
    'Makawanpur',
    'Nuwakot',
    'Ramechhap',
    'Rasuwa',
    'Sindhuli',
    'Sindhupalchok',
  ];
  List<String> provinceFourDistrict = [
    'Baglung',
    'Gorkha',
    'Kaski',
    'Lamjung',
    'Manang',
    'Mustang',
    'Myagdi',
    'Nawalpur',
    'Parbat',
    'Syangja',
    'Tanahu'
  ];
  List<String> provinceFiveDistrict = [
    'Arghakhanchi',
    'Banke',
    'Bardiya',
    'Dang',
    'Gulmi',
    'Kapilvastu',
    'Parasi',
    'Palpa',
    'Pyuthan',
    'Rolpa',
    'Rukum',
    'Rupandehi',
  ];

  @override
  void initState() {
    buttonPressed = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Sign Up Page'),
        centerTitle: true,
      ),
      body: Form(
        key: formkey,
        child: ListView(
          shrinkWrap: true,
          children: [
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30,
              ),
              child: CustomFormField(
                controller: firstNameController,
                errormessage: 'Enter First Name',
                inputType: TextInputType.name,
                lableText: 'First Name',
                obscureText: false,
                prefixIcon: Icons.person,
                iconssuffix: null,
                lengthError: 'Enter First Name',
                maxlength: 15,
                len: 3,
                suffixicon: null,
              ),
            ),
            SizedBox(
              height: gap,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30,
              ),
              child: CustomFormField(
                controller: lastNameController,
                errormessage: 'Enter Last Name',
                inputType: TextInputType.name,
                lableText: 'Last Name',
                obscureText: false,
                prefixIcon: Icons.person,
                iconssuffix: null,
                lengthError: 'Enter Last Name',
                maxlength: 12,
                len: 5,
                suffixicon: null,
              ),
            ),
            SizedBox(
              height: gap,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30,
              ),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                validator: (email) =>
                    email != null && !EmailValidator.validate(email)
                        ? 'Check Your Email'
                        : null,
                controller: emailController,
                obscureText: false,
                decoration: InputDecoration(
                  isDense: true,
                  contentPadding: const EdgeInsets.all(10),
                  focusedErrorBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(
                      color: Colors.red[500]!,
                      width: 0.5,
                    ),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(
                      color: Colors.blue[500]!,
                      width: 0.5,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15),
                    borderSide: BorderSide(
                      color: Colors.grey[500]!,
                      width: 0.5,
                    ),
                  ),
                  prefixIcon: Icon(
                    Icons.email_outlined,
                    color: Colors.grey[400],
                    size: 18,
                  ),
                  labelText: "Email",
                  border: const OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 40.0,
                      color: Color(0xFFFFFFFF),
                    ),
                    borderRadius: BorderRadius.all(
                      Radius.circular(15),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: gap,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30,
              ),
              child: CustomFormField(
                controller: phoneController,
                lableText: 'Phone Number',
                errormessage: 'Enter Phone Number',
                iconssuffix: null,
                prefixIcon: Icons.phone_android,
                obscureText: false,
                inputType: TextInputType.phone,
                prefixtext: '+977',
                len: 10,
                maxlength: 10,
                lengthError: 'Enter Correct Phone Number',
              ),
            ),
            SizedBox(
              height: gap,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 30,
              ),
              child: CustomFormField(
                  controller: passwordController,
                  lableText: 'Password',
                  errormessage: 'Enter Password',
                  iconssuffix: InkWell(
                    onTap: () {
                      if (isHidden == true) {
                        isHidden = false;
                        icon = Icons.visibility;
                      } else {
                        isHidden = true;
                        icon = Icons.visibility_off_outlined;
                      }
                      setState(() {});
                    },
                    child: Icon(
                      icon,
                      color: Colors.grey,
                    ),
                  ),
                  prefixIcon: Icons.lock,
                  obscureText: isHidden,
                  inputType: TextInputType.emailAddress,
                  len: 8,
                  lengthError: 'Enter Atleast 8 character'),
            ),
            SizedBox(
              height: gap,
            ),
            Container(
              margin: const EdgeInsets.symmetric(horizontal: 30),
              padding: const EdgeInsets.all(10),
              height: 50,
              decoration: BoxDecoration(
                border: buttonPressed == true
                    ? selectedGender == null
                        ? Border.all(width: 1, color: Colors.red[700]!)
                        : Border.all(width: 0.5, color: Colors.grey[400]!)
                    : Border.all(width: 0.5, color: Colors.grey[400]!),
                borderRadius: BorderRadius.circular(15),
              ),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  hint: const Text(
                    'Select Gender',
                  ),
                  isExpanded: true,
                  value: selectedGender,
                  icon: const Icon(
                    Icons.arrow_drop_down,
                  ),
                  items: gender.map((String values) {
                    return DropdownMenuItem<String>(
                      value: values,
                      child: Text(values),
                    );
                  }).toList(),
                  onChanged: (gender) {
                    setState(() {
                      selectedGender = gender;
                      genderError = null;
                    });
                  },
                ),
              ),
            ),
            SizedBox(
              height: size.height * .008,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 40,
              ),
              child: Text(
                genderError ?? "",
                style: TextStyle(
                  color: Colors.red[700],
                  fontSize: 12,
                ),
              ),
            ),
            SizedBox(
              height: gap,
            ),
            Padding(
              padding: const EdgeInsets.only(
                top: 0,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //Province Drop Down
                  Container(
                    margin: const EdgeInsets.symmetric(horizontal: 30),
                    padding: const EdgeInsets.all(10),
                    height: 50,
                    decoration: BoxDecoration(
                      border: buttonPressed == true
                          ? selectedProvince == null
                              ? Border.all(width: 1, color: Colors.red[700]!)
                              : Border.all(width: 0.5, color: Colors.grey[400]!)
                          : Border.all(width: 0.5, color: Colors.grey[400]!),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                          hint: const Text(
                            'Select Province',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                          value: selectedProvince,
                          isExpanded: true,
                          icon: const Icon(
                            Icons.arrow_drop_down,
                          ),
                          items: provinces
                              .map<DropdownMenuItem<String>>((String values) {
                            return DropdownMenuItem<String>(
                              value: values,
                              child: Text(
                                values,
                                style: const TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                            );
                          }).toList(),
                          onChanged: (province) {
                            if (province == 'Province No. 1') {
                              districts = provinceOneDistrict;
                            } else if (province == 'Province No. 2') {
                              districts = provinceTwoDistrict;
                            } else if (province == 'Province No. 3') {
                              districts = provinceThreeDistrict;
                            } else if (province == 'Province No. 4') {
                              districts = provinceFourDistrict;
                            } else if (province == 'Province No. 5') {
                              districts = provinceFiveDistrict;
                            } else {
                              districts = [];
                            }
                            setState(() {
                              selectedDistrict = null;
                              selectedProvince = province;
                              provinceError = null;
                            });
                          }),
                    ),
                  ),
                  SizedBox(
                    height: gap,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 40,
                    ),
                    child: Text(
                      provinceError ?? "",
                      style: TextStyle(
                        color: Colors.red[700]!,
                        fontSize: 12,
                      ),
                    ),
                  ),
                  SizedBox(
                    height: gap,
                  ),
                  //District Drop Down
                  Container(
                    margin: const EdgeInsets.symmetric(
                      horizontal: 30,
                      // vertical: 10,
                    ),
                    padding: const EdgeInsets.all(10),
                    height: 50,
                    decoration: BoxDecoration(
                      border: buttonPressed == true
                          ? selectedDistrict == null
                              ? Border.all(width: 1, color: Colors.red[700]!)
                              : Border.all(width: 0.5, color: Colors.grey[400]!)
                          : Border.all(width: 0.5, color: Colors.grey[400]!),
                      borderRadius: BorderRadius.circular(15),
                    ),
                    child: DropdownButtonHideUnderline(
                      child: DropdownButton<String>(
                          hint: const Text(
                            'Districts',
                            style: TextStyle(
                              fontSize: 14,
                            ),
                          ),
                          value: selectedDistrict,
                          isExpanded: true,
                          items: districts.map((String values) {
                            return DropdownMenuItem<String>(
                              value: values,
                              child: Text(
                                values,
                                style: const TextStyle(
                                  fontSize: 14,
                                ),
                              ),
                            );
                          }).toList(),
                          onChanged: (district) {
                            setState(() {
                              selectedDistrict = district;
                              districtError = null;
                            });
                          }),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 40,
                    ),
                    child: Text(
                      districtError ?? "",
                      style: TextStyle(
                        color: Colors.red[700]!,
                        fontSize: 12,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: gap,
            ),
            Padding(
              padding: const EdgeInsets.only(
                left: 30,
                right: 30,
                top: 2,
              ),
              child: CustomFormField(
                lengthError: "Enter Correct Local Address",
                len: 4,
                inputType: TextInputType.name,
                controller: addressController,
                errormessage: "Enter Local Address",
                lableText: "Local Address",
                obscureText: false,
                prefixIcon: null,
                suffixicon: Icons.location_on_outlined,
                iconssuffix: null,
              ),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(
                horizontal: 25,
              ),
              child: ElevatedButton(
                style: ButtonStyle(
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15),
                    ),
                  ),
                ),
                onPressed: () {
                  setState(() {
                    buttonPressed = true;
                  });
                  _validateForm();
                },
                child: const Padding(
                  padding: EdgeInsets.symmetric(vertical: 16),
                  child: Text('Register'),
                ),
              ),
            ),
            const SizedBox(
              height: 20,
            ),
          ],
        ),
      ),
    );
  }
}
